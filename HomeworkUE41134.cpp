﻿// HomeworkUE41134.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "Helpers.h"
#include <iostream>

int main()
{
    std::cout << "The sum squared of two numbers:\n";
    std::cout << SumSquared(25, 33) << "\n";
    return 0;
}
